<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../include/_header.jsp"/>


   <h1>PROJECT EDIT</h1>

        <form:form method="POST" action="/projects/update/${project.id}" modelAttribute="project">
            <form:label path="name">Name</form:label>
            <form:input path="name"/>

            <form:label path="description">Description</form:label>
            <form:input path="description"/>

            <input type="submit" value="Update" class="customButton">
        </form:form>

<jsp:include page="../include/_footer.jsp"/>
