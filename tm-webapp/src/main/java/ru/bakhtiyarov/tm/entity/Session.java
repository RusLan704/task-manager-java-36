package ru.bakhtiyarov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "tm_session")
public final class Session extends AbstractEntity implements Cloneable {

    @Nullable
    @Column(nullable = false)
    private Long timestamp;

    @Nullable
    @Column(nullable = false)
    private String userId;

    @Nullable
    @Column(nullable = false)
    private String signature;

    @Nullable
    @ManyToOne
    private User user;

    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

}
