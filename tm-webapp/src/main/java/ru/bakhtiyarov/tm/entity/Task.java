package ru.bakhtiyarov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Setter
@Getter
@NoArgsConstructor
@Table(name = "tm_task")
public final class Task extends AbstractEntity {

    @NotNull
    @Column(nullable = false, columnDefinition = "TEXT")
    private String name = "";

    @Nullable
    @Column(columnDefinition = "TEXT")
    private String description = "";

    @Nullable
    private Date startDate;

    @Nullable
    private Date completeDate;

    @Nullable
    @Column(updatable = false)
    private Date creationDate = new Date(System.currentTimeMillis());

    @Nullable
    @ManyToOne
    private User user;

    @Nullable
    @ManyToOne
    private Project project;

    @NotNull
    public Task(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    @NotNull
    public Task(@NotNull String name) {
        this.name = name;
    }

}
