package ru.bakhtiyarov.tm.exception.invalid;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.exception.AbstractException;

public class InvalidLoginException extends AbstractException {

    @NotNull
    public InvalidLoginException(@NotNull final String arg) {
        super("Error! Login ```" + arg + "``` is busy...");
    }

}
