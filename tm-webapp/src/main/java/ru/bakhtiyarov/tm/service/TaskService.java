package ru.bakhtiyarov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.bakhtiyarov.tm.api.service.IProjectService;
import ru.bakhtiyarov.tm.api.service.ITaskService;
import ru.bakhtiyarov.tm.api.service.IUserService;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.entity.Task;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.exception.empty.EmptyDescriptionException;
import ru.bakhtiyarov.tm.exception.empty.EmptyIdException;
import ru.bakhtiyarov.tm.exception.empty.EmptyNameException;
import ru.bakhtiyarov.tm.exception.empty.EmptyUserIdException;
import ru.bakhtiyarov.tm.exception.invalid.InvalidProjectException;
import ru.bakhtiyarov.tm.exception.invalid.InvalidTaskException;
import ru.bakhtiyarov.tm.exception.invalid.InvalidUserException;
import ru.bakhtiyarov.tm.exception.system.IncorrectIndexException;
import ru.bakhtiyarov.tm.repository.ITaskRepository;

import java.util.List;
import java.util.Optional;

@Service
public class TaskService extends AbstractService<Task, ITaskRepository> implements ITaskService {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final IUserService userService;

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    @Autowired
    public TaskService(
           @NotNull final IProjectService projectService,
           @NotNull final IUserService userService, 
           @NotNull final ITaskRepository taskRepository
    ) {
        this.projectService = projectService;
        this.userService = userService;
        this.taskRepository = taskRepository;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(@Nullable final String userId, @Nullable String projectName, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable User user = userService.findById(userId);
        if (user == null) throw new InvalidUserException();
        @Nullable Project project = projectService.findOneByName(userId, projectName);
        if (project == null) throw new InvalidProjectException();
        @NotNull Task task = new Task(name);
        task.setUser(user);
        task.setProject(project);
        taskRepository.save(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(@Nullable final String userId, @Nullable String projectName, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable User user = userService.findById(userId);
        if (user == null) throw new InvalidUserException();
        @Nullable Project project = projectService.findOneByName(userId, projectName);
        if (project == null) throw new InvalidProjectException();
        @NotNull Task task = new Task(name, description);
        task.setUser(user);
        task.setProject(project);
        taskRepository.save(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        taskRepository.deleteAllByUserId(userId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @Nullable Task task = taskRepository.findAllByUserId(userId).get(index);
        if(task == null) throw new InvalidTaskException();
        taskRepository.deleteById(task.getId());
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return taskRepository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByUserIdAndProjectName(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectName == null || projectName.isEmpty()) throw new EmptyIdException();
        @Nullable final Project project = projectService.findOneByName(userId, projectName);
        if (project == null) throw new InvalidProjectException();
        return taskRepository.findAllByUserIdAndProjectName(userId, projectName);
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public void removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        taskRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    public void removeOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        taskRepository.deleteById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        taskRepository.deleteByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    public void removeAll() {
        taskRepository.deleteAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeAllByUserIdAndProjectName(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectName == null || projectName.isEmpty()) throw new EmptyNameException();
        @Nullable final Project project = projectService.findOneByName(userId, projectName);
        if (project == null) throw new InvalidProjectException();
        taskRepository.deleteAllByUserIdAndProjectId(userId, projectName);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task updateTaskById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new  InvalidTaskException();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return save(task);
    }

    @Nullable
    @SneakyThrows
    @Transactional
    public Task updateTaskById(
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable final Task task = findById(id);
        if (task == null) throw new  InvalidTaskException();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return save(task);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneById(@Nullable final String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task updateTaskByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return taskRepository.save(task);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return taskRepository.findAllByUserId(userId).get(index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByUserIdAndName(userId, name);
    }

    @Override
    public Task findById(@Nullable final String id) {
        return taskRepository.findById(id).get();
    }

    @NotNull
    @Override
    protected ITaskRepository getRepository() {
        return taskRepository;
    }

}