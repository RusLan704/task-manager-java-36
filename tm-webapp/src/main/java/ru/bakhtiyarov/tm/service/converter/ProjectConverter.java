package ru.bakhtiyarov.tm.service.converter;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.bakhtiyarov.tm.api.service.converter.IProjectConverter;
import ru.bakhtiyarov.tm.dto.ProjectDTO;
import ru.bakhtiyarov.tm.entity.Project;

@Service
public class ProjectConverter implements IProjectConverter {

    @Nullable
    @Override
    public ProjectDTO toDTO(@Nullable final Project project) {
        if (project == null) return null;
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(project.getId());
        projectDTO.setName(project.getName());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setUserId(project.getUser().getId());
        return projectDTO;
    }

    @Nullable
    @Override
    public Project toEntity(@Nullable final ProjectDTO projectDTO) {
        if (projectDTO == null) return null;
        Project project = new Project();
        project.setId(projectDTO.getId());
        project.setName(projectDTO.getName());
        project.setDescription(projectDTO.getDescription());
        return project;
    }

}
