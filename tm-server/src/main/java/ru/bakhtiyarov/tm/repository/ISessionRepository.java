package ru.bakhtiyarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.bakhtiyarov.tm.entity.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    @NotNull
    List<Session> findAllByUserId(@NotNull String userId);

    void deleteAllByUserId(@NotNull String userId);

    void deleteByUserId(@NotNull String userId);

}
