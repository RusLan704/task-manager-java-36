package ru.bakhtiyarov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.bakhtiyarov.tm.api.endpoint.IAdminDataEndpoint;
import ru.bakhtiyarov.tm.api.service.IDomainService;
import ru.bakhtiyarov.tm.api.service.ISessionService;
import ru.bakhtiyarov.tm.api.service.converter.ISessionConverter;
import ru.bakhtiyarov.tm.dto.SessionDTO;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.enumeration.Role;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
public class AdminDataEndpoint extends AbstractEndpoint implements IAdminDataEndpoint {

    @NotNull
    private final ISessionConverter sessionConverter;

    @NotNull
    private final IDomainService domainService;

    @NotNull
    @Autowired
    public AdminDataEndpoint(
            @NotNull final ISessionConverter sessionConverter,
            @NotNull final IDomainService domainService
    ) {
        this.sessionConverter = sessionConverter;
        this.domainService = domainService;
    }

    @Override
    @SneakyThrows
    public void loadBase64(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session, Role.ADMIN);
        domainService.loadBase64();
    }

    @Override
    @SneakyThrows
    public void clearBase64(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session, Role.ADMIN);
        domainService.clearBase64();
    }

    @Override
    @SneakyThrows
    public void saveBase64(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session, Role.ADMIN);
        domainService.saveBase64();
    }

    @Override
    @SneakyThrows
    public void clearBinary(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session, Role.ADMIN);
        domainService.clearBinary();
    }

    @Override
    @SneakyThrows
    public void loadBinary(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session, Role.ADMIN);
        domainService.loadBinary();
    }

    @Override
    @SneakyThrows
    public void saveBinary(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session, Role.ADMIN);
        domainService.saveBinary();
    }

    @Override
    @SneakyThrows
    public void clearJson(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session, Role.ADMIN);
        domainService.clearJson();
    }

    @Override
    @SneakyThrows
    public void loadJson(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session, Role.ADMIN);
        domainService.loadJson();
    }

    @Override
    @SneakyThrows
    public void saveJson(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session, Role.ADMIN);
        domainService.saveJson();
    }

    @Override
    @SneakyThrows
    public void clearXml(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session, Role.ADMIN);
        domainService.clearXml();
    }

    @Override
    @SneakyThrows
    public void loadXml(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session, Role.ADMIN);
        domainService.loadXml();
    }

    @Override
    @SneakyThrows
    public void saveXml(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session, Role.ADMIN);
        domainService.saveXml();
    }

    @Override
    @SneakyThrows
    public void clearYaml(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session, Role.ADMIN);
        domainService.clearYaml();
    }

    @Override
    @SneakyThrows
    public void loadYaml(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session, Role.ADMIN);
        domainService.loadYaml();
    }

    @Override
    @SneakyThrows
    public void saveYaml(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session, Role.ADMIN);
        domainService.saveYaml();
    }

}
