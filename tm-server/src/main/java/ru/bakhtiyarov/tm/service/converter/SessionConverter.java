package ru.bakhtiyarov.tm.service.converter;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.bakhtiyarov.tm.api.service.converter.ISessionConverter;
import ru.bakhtiyarov.tm.dto.SessionDTO;
import ru.bakhtiyarov.tm.entity.Session;

@Service
public class SessionConverter implements ISessionConverter {

    @Nullable
    @Override
    public SessionDTO toDTO(@Nullable final Session session) {
        if (session == null) return null;
        SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setId(session.getId());
        sessionDTO.setUserId(session.getUserId());
        sessionDTO.setSignature(session.getSignature());
        sessionDTO.setTimestamp(session.getTimestamp());
        return sessionDTO;
    }

    @Nullable
    @Override
    public Session toEntity(@Nullable final SessionDTO sessionDTO) {
        if (sessionDTO == null) return null;
        Session session = new Session();
        session.setId(sessionDTO.getId());
        session.setUserId(sessionDTO.getUserId());
        session.setSignature(sessionDTO.getSignature());
        session.setTimestamp(sessionDTO.getTimestamp());
        return session;
    }
    
}
