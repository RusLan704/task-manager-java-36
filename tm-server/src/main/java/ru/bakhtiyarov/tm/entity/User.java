package ru.bakhtiyarov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.enumeration.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Setter
@Getter
@NoArgsConstructor
@Table(name = "tm_user")
public final class User extends AbstractEntity {

    public static final long serialVersionUID = 1L;

    @NotNull
    @Column(unique = true, nullable = false)
    private String login = "";

    @NotNull
    @Column(nullable = false)
    private String passwordHash = "";

    @NotNull
    @Column
    private String email = "";

    @NotNull
    @Column
    private String firstName = "";

    @NotNull
    @Column
    private String lastName = "";

    @NotNull
    @Column
    private String middleName = "";

    @Nullable
    @Column
    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;

    @Column(nullable = false)
    private Boolean locked = false;

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Project> projects = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Task> sessions = new ArrayList<>();

    @NotNull
    public User(@NotNull String login, @NotNull String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    @NotNull
    public User(@NotNull String login, @NotNull String passwordHash, @Nullable Role role) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.role = role;
    }

}
