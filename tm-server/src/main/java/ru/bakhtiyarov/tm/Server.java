package ru.bakhtiyarov.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.AbstractApplicationContext;
import ru.bakhtiyarov.tm.bootstrap.Bootstrap;
import ru.bakhtiyarov.tm.config.ServerConfiguration;

@ComponentScan
public class Server {

    public static void main(String... args) {
        @NotNull final AnnotationConfigApplicationContext context
                = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        context.registerShutdownHook();
        Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run();
    }

}

