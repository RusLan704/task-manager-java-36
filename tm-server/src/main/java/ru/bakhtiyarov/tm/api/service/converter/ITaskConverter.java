package ru.bakhtiyarov.tm.api.service.converter;

import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.dto.TaskDTO;
import ru.bakhtiyarov.tm.entity.Task;

public interface ITaskConverter {

    @Nullable
    TaskDTO toDTO(@Nullable Task task);

    @Nullable
    Task toEntity(@Nullable TaskDTO taskDTO);

}
