package ru.bakhtiyarov.tm.repository;

import org.junit.experimental.categories.Category;
import ru.bakhtiyarov.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public final class ProjectDTORepositoryTest {

 /*   @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private ProjectDTO projectDto;

    @Before
    public void addProject() {
        projectDto = new ProjectDTO("project", "description", "1");
        projectRepository.add(projectDto);
    }

    @After
    public void removeProject() {
        projectRepository.clearAll();
    }

    @Test
    public void testAdd() {
        ProjectDTO projectDTOAdded = projectRepository.findOneById(projectDto.getUserId(), projectDto.getId());
        Assert.assertNotNull(projectDTOAdded);
        Assert.assertEquals(projectDto.getName(), projectDTOAdded.getName());
        Assert.assertEquals(projectDto.getDescription(), projectDTOAdded.getDescription());
        Assert.assertEquals(projectDto.getUserId(), projectDTOAdded.getUserId());
    }

    @Test
    public void testClear() {
        Assert.assertEquals(1, projectRepository.findAll().size());
        projectRepository.add(projectDto);
        projectRepository.add(projectDto);
        projectRepository.add(projectDto);
        projectRepository.clear(projectDto.getUserId());
        Assert.assertEquals(0, projectRepository.findAll().size());
    }

    @Test
    public void testRemove() {
        Assert.assertEquals(1, projectRepository.findAll().size());
        projectRepository.remove(projectDto);
        Assert.assertEquals(0, projectRepository.findAll().size());
    }

    @Test
    public void testRemoveByIndex() {
        Assert.assertEquals(1, projectRepository.findAll().size());
        ProjectDTO projectDTOTest = projectRepository.removeOneByIndex(ConstTest.FALSE_USER_ID, 0);
        Assert.assertNull(projectDTOTest);
        projectRepository.removeOneByIndex(projectDto.getUserId(), 0);
        Assert.assertEquals(0, projectRepository.findAll().size());
    }

    @Test
    public void testRemoveByName() {
        Assert.assertEquals(1, projectRepository.findAll().size());
        Assert.assertNull(projectRepository.removeOneByName(ConstTest.FALSE_USER_ID, projectDto.getName()));
        Assert.assertNull(projectRepository.removeOneByName(projectDto.getUserId(), ConstTest.FALSE_NAME));
        projectRepository.removeOneByName(projectDto.getUserId(), projectDto.getName());
        Assert.assertEquals(0, projectRepository.findAll().size());
    }

    @Test
    public void testRemoveById() {
        Assert.assertEquals(1, projectRepository.findAll().size());
        projectRepository.removeOneById(projectDto.getUserId(), projectDto.getId());
        Assert.assertEquals(0, projectRepository.findAll().size());
    }

    @Test
    public void findAll() {
        Assert.assertEquals(1, projectRepository.findAll().size());
        projectRepository.add(projectDto);
        projectRepository.add(projectDto);
        projectRepository.add(projectDto);
        projectRepository.add(projectDto);
        final List<ProjectDTO> projectDTOS = projectRepository.findAll(projectDto.getUserId());
        Assert.assertNotNull(projectDTOS);
        Assert.assertEquals(5, projectRepository.findAll().size());
    }

    @Test
    public void findOneByIndex() {
        Assert.assertEquals(1, projectRepository.findAll().size());
        ProjectDTO projectDTOTest = projectRepository.findOneByIndex(projectDto.getUserId(), 0);
        Assert.assertNotNull(projectDTOTest);
        Assert.assertEquals(projectDto.getName(), projectDTOTest.getName());
        Assert.assertEquals(projectDto.getUserId(), projectDTOTest.getUserId());
        ProjectDTO nullProjectDTO = projectRepository.findOneByIndex(projectDto.getUserId(), 10);
        Assert.assertNull(nullProjectDTO);
    }

    @Test
    public void findOneById() {
        Assert.assertEquals(1, projectRepository.findAll().size());
        ProjectDTO projectDTOTest = projectRepository.findOneById(projectDto.getUserId(), projectDto.getId());
        Assert.assertNotNull(projectDTOTest);
        Assert.assertEquals(projectDto.getUserId(), projectDTOTest.getUserId());
        Assert.assertEquals(projectDto.getName(), projectDTOTest.getName());
        ProjectDTO nullProjectDTO = projectRepository.findOneById(projectDto.getUserId(), ConstTest.FALSE_ID);
        Assert.assertNull(nullProjectDTO);
    }

    @Test
    public void findOneByName() {
        Assert.assertEquals(1, projectRepository.findAll().size());
        ProjectDTO projectDTOTest = projectRepository.findOneByName(projectDto.getUserId(), projectDto.getName());
        Assert.assertNotNull(projectDTOTest);
        Assert.assertEquals(projectDto.getUserId(), projectDTOTest.getUserId());
        Assert.assertEquals(projectDto.getName(), projectDTOTest.getName());
        ProjectDTO nullProjectDTO = projectRepository.findOneById(projectDto.getUserId(), ConstTest.FALSE_NAME);
        Assert.assertNull(nullProjectDTO);
    }
*/
}
