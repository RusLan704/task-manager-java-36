package ru.bakhtiyarov.tm.service;

import org.junit.experimental.categories.Category;
import ru.bakhtiyarov.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public class AbstractServiceTest {

 /*   @NotNull
    private final AbstractRepository<AbstractEntityDTOTest> abstractRepository = new AbstractRepositoryTest();

    @NotNull
    private final AbstractService<AbstractEntityDTOTest> abstractService = new AbstractServiceTestable(abstractRepository);

    @Test
    public void testAddAll() {
        Assert.assertEquals(0, abstractService.findAll().size());
        List<AbstractEntityDTOTest> lists = Arrays.asList
                (
                        new AbstractEntityDTOTest("name"),
                        new AbstractEntityDTOTest("name"),
                        new AbstractEntityDTOTest("name")
                );
        abstractService.addAll(lists);
        Assert.assertEquals(3, abstractService.findAll().size());
    }

    @Test
    public void testClearAll() {
        Assert.assertEquals(0, abstractService.findAll().size());
        List<AbstractEntityDTOTest> lists = Arrays.asList
                (
                        new AbstractEntityDTOTest("name"),
                        new AbstractEntityDTOTest("name"),
                        new AbstractEntityDTOTest("name")
                );
        abstractService.addAll(lists);
        Assert.assertEquals(3, abstractService.findAll().size());
        abstractService.clearAll();
        Assert.assertEquals(0, abstractService.findAll().size());
    }

    @Test
    public void testFindById() {
        Assert.assertEquals(0, abstractService.findAll().size());
        AbstractEntityDTOTest abstractEntityTest = new AbstractEntityDTOTest("name");
        abstractRepository.add(abstractEntityTest);
        AbstractEntityDTOTest tempAbstractEntityTest = abstractService.findById(abstractEntityTest.getId());
        Assert.assertNotNull(tempAbstractEntityTest);
        Assert.assertEquals(abstractEntityTest.getId(), tempAbstractEntityTest.getId());
        Assert.assertEquals(abstractEntityTest.getName(), tempAbstractEntityTest.getName());
    }

    @Test
    public void testRemoveById() {
        Assert.assertEquals(0, abstractService.findAll().size());
        AbstractEntityDTOTest abstractEntityTest = new AbstractEntityDTOTest("name");
        abstractRepository.add(abstractEntityTest);
        Assert.assertEquals(1, abstractService.findAll().size());
        AbstractEntityDTOTest tempAbstractEntityTest = abstractService.removeById(abstractEntityTest.getId());
        Assert.assertEquals(0, abstractService.findAll().size());
        Assert.assertNotNull(tempAbstractEntityTest);
        Assert.assertEquals(abstractEntityTest.getId(), tempAbstractEntityTest.getId());
        Assert.assertEquals(abstractEntityTest.getName(), tempAbstractEntityTest.getName());
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeFindByIdWithEmptyId() {
        abstractService.findById("");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeFindByIdWithNullId() {
        abstractService.findById(null);
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeRemoveByIdWithEmptyId() {
        abstractService.removeById("");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeRemoveByIdWithNullId() {
        abstractService.removeById(null);
    }

    private static final class AbstractRepositoryTest extends AbstractRepository<AbstractEntityDTOTest> {
    }

    private static final class AbstractServiceTestable extends AbstractService<AbstractEntityDTOTest> {

        public AbstractServiceTestable(@NotNull final IRepository<AbstractEntityDTOTest> repository) {
            super(repository);
        }

    }

    @Getter
    @Setter
    @AllArgsConstructor
    private static final class AbstractEntityDTOTest extends AbstractEntityDTO {

        @Nullable
        private String name;

    }
*/
}
