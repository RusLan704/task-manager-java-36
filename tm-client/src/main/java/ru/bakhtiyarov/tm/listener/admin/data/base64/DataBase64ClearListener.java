package ru.bakhtiyarov.tm.listener.admin.data.base64;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.listener.AbstractListener;
import ru.bakhtiyarov.tm.endpoint.AdminDataEndpoint;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;

@Component
public final class DataBase64ClearListener extends AbstractListener {

    @Autowired
    private AdminDataEndpoint adminDataEndpoint;
    
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-base64-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Clear base64 file.";
    }

    @EventListener(condition = "@dataBase64ClearListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[DATA BASE64 CLEAR]");
        @NotNull SessionDTO session = sessionService.getSession();
        adminDataEndpoint.clearBase64(session);
        System.out.println("[OK]");
    }

}
