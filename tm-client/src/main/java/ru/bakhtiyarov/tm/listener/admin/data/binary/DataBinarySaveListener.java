package ru.bakhtiyarov.tm.listener.admin.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.listener.AbstractListener;
import ru.bakhtiyarov.tm.endpoint.AdminDataEndpoint;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;

@Component
public final class DataBinarySaveListener extends AbstractListener {

    @Autowired
    private AdminDataEndpoint adminDataEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-bin-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data from binary file.";
    }

    @Override
    @EventListener(condition = "@dataBinarySaveListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[DATA BINARY SAVE]");
        @NotNull SessionDTO session = sessionService.getSession();
        adminDataEndpoint.saveBinary(session);
        System.out.println("[OK]");
    }

}