package ru.bakhtiyarov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.listener.AbstractListener;

import java.util.List;

@Component
public final class ArgumentsListener extends AbstractListener {

    @Autowired
    private List<AbstractListener> commandList;

    @NotNull
    @Override
    public String arg() {
        return "-arg";
    }

    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

    @NotNull
    @Override
    public String description() {
        return "Show program arguments.";
    }

    @Override
    @EventListener(condition = "@argumentsListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        for (@NotNull final AbstractListener command : commandList) {
            if (command.arg() == null) continue;
            System.out.println(command.arg());
        }
    }

}
