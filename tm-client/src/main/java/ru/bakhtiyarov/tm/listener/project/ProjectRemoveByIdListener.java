package ru.bakhtiyarov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.listener.AbstractListener;
import ru.bakhtiyarov.tm.constant.TerminalConst;
import ru.bakhtiyarov.tm.endpoint.ProjectDTO;
import ru.bakhtiyarov.tm.endpoint.ProjectEndpoint;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;
import ru.bakhtiyarov.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveByIdListener extends AbstractListener {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return TerminalConst.PROJECT_REMOVE_BY_ID;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by id.";
    }

    @EventListener(condition = "@projectRemoveByIdListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull SessionDTO session = sessionService.getSession();
        projectEndpoint.removeProjectOneById(session, id);
        System.out.println("[OK]");
    }

}
