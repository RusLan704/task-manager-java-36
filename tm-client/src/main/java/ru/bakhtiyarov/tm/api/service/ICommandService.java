package ru.bakhtiyarov.tm.api.service;

import ru.bakhtiyarov.tm.listener.AbstractListener;

import java.util.List;

public interface ICommandService {

    List<AbstractListener> getCommandList();

}
